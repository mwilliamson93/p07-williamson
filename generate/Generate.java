import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;

/**
 * Program to generate a dictionary of Ghost friendly words
 * Dictionary is grabbed from a file that is on most unix machine
 *
 * @author Matthew Williamson
 */
public class Generate {
	public static void main(String args[]) {
		int count = 0;
		Trie dict = new Trie();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("/usr/share/dict/words")));
			PrintWriter writer = new PrintWriter("dict.txt", "UTF-8");
			String word;
			while ((word = br.readLine()) != null) {
				if (!dict.containsPrefix(word) && 3 < word.length() && Character.isLowerCase(word.charAt(0))) {
					dict.insert(word);
					writer.println(word);
					count++;
				}
			}
			writer.close();
			br.close();
		}
		catch (Exception e) {
			System.err.println("File not found exception.");
		}
		System.out.println("Count: " + count);
	}
}

