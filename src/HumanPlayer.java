import java.io.InputStreamReader;
import java.io.BufferedReader;

/**
 * Human player is the player type for a human
 *
 * @author Matthew Williamson
 */
public class HumanPlayer implements Player {
	private final String name;
	private Trie dict;

	/**
	 * Constructor for the human player
	 *
	 * @param name The name of the player
	 */
	public HumanPlayer(String name) {
		this.name = name;
	}

	/**
	 * Getter style method to choose the name of the player
	 *
	 * @return The name
	 */
	public String name() {
		return this.name;
	}

	// TODO: Refactor for Android
	/**
	 * Play acts as the method in which a player chooses a letter
	 *
	 * @param word Current word being created in the game
	 * @return     The letter choice made by the player
	 */
	public char play(String word) {
		while (true) {
			if (word.length() == 0) {
				System.out.print(this.name + " pick the first letter: ");
			}
			else {
				System.out.print(this.name + " pick the next letter: " + word);
			}

			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				return br.readLine().charAt(0);
			}
			catch (Exception e) {
				System.err.println("\nYou must type in a letter.");
			}
		}
	}
}

